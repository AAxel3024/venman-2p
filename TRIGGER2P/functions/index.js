const functions = require('firebase-functions');
const admin=require('firebase-admin');
admin.initializeApp();
const db=admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.onUserCreate = functions.firestore.document('mantenimiento/{mantenimientoId}').onCreate(async (snap, context) => {
    const values = snap.data();
    const query = db.collection("mantenimiento");
    const snapshot = await query.where("tecnico_id","==",values.tecnico_id).get();
    const count = snapshot.size;
 
    if(count > 2){
            try {
                const res = await db.collection('mantenimiento').doc(snap.id).delete();
                console.log(`No se aceptan más mantenimientos ya  existen dos mantenimiento activos`);
            } catch (error) {
              console.log(error);  
            }
    }
})