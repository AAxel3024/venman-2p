------------------------------------------------------------TRANSACCIÓN #1----------------------------------------
CREATE OR REPLACE PROCEDURE actualizar_datos(
	integer,
	integer,
	integer,
	character varying,
	date,
	date,
	integer,
	character varying
)
LANGUAGE 'plpgsql'
AS 
$BODY$
	declare 
	ant varchar;
	act varchar;
	begin
	insert into mantenimiento (tecnicos_id, cliente_id, tipo_id, garantia_mantenimiento,fecha_recepcion,fecha_entrega,costo_mantenimiento,estado_mantenimiento) values ($1,$2,$3,$4,$5,$6,$7,$8);
	select tipo_mantenimiento into act from tipo_mantenimiento where tipo_id = $3;
	select mantenimiento_act into ant from tecnico where tecnicos_id = $1;
	update tecnico set mantenimiento_act = act, mantenimiento_ant = ant  where tecnicos_id = $1;
	exception 
	when sqlstate '23503' then 
	raise exception 'Error en la transaccion, no existe el tipo de mantenimiento';
	rollback;
	commit;
end;
$BODY$

SELECT * FROM TECNICO
SELECT * FROM MANTENIMIENTO 
select * from cliente
SELECT * FROM TIPO_MANTENIMIENTO 
CALL actualizar_datos (4, 500, 117, '6meses', '2022/01/22', '2022/01/30', 91, 'BIEN')
CALL actualizar_datos (3, 500, 113)