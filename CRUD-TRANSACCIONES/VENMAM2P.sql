
/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   CLIENTE_ID           SERIAL               not null,
   LUGAR_ID             INT4                 not null,
   VENTAS_ID            INT4                 not null,
   NOMBRE_CLIENTE       VARCHAR(30)          null,
   APELLIDO_CLIENTE     VARCHAR(30)          null,
   CEDULA_CLIENTE       VARCHAR(10)          null,
   NUM_TELEFONO_CLIENTE VARCHAR(10)          null,
   CORREO_CLIENTE       VARCHAR(35)          null,
   GENERO_CLIENTE       VARCHAR(9)           null,
   DIRECCION_CLIENTE    VARCHAR(50)          null,
   constraint PK_CLIENTE primary key (CLIENTE_ID)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
CLIENTE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on CLIENTE (
LUGAR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on CLIENTE (
VENTAS_ID
);

/*==============================================================*/
/* Table: COMPRA                                                */
/*==============================================================*/
create table COMPRA (
   COMPRAS_ID           SERIAL               not null,
   TOTAL_COMPRA         INT4                 null,
   FECHA_COMPRA         DATE                 null,
   constraint PK_COMPRA primary key (COMPRAS_ID)
);

/*==============================================================*/
/* Index: COMPRA_PK                                             */
/*==============================================================*/
create unique index COMPRA_PK on COMPRA (
COMPRAS_ID
);

/*==============================================================*/
/* Table: LINE_COMPRA                                           */
/*==============================================================*/
create table LINE_COMPRA (
   PROVEEDOR_ID         INT4                 not null,
   COMPRAS_ID           INT4                 not null,
   constraint PK_LINE_COMPRA primary key (PROVEEDOR_ID, COMPRAS_ID)
);

/*==============================================================*/
/* Index: LINE_COMPRA_PK                                        */
/*==============================================================*/
create unique index LINE_COMPRA_PK on LINE_COMPRA (
PROVEEDOR_ID,
COMPRAS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_13_FK on LINE_COMPRA (
COMPRAS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_11_FK on LINE_COMPRA (
PROVEEDOR_ID
);

/*==============================================================*/
/* Table: LUGAR                                                 */
/*==============================================================*/
create table LUGAR (
   LUGAR_ID             SERIAL               not null,
   PAIS_CLIENTE         VARCHAR(25)          null,
   CIUDAD_CLIENTE       VARCHAR(25)          null,
   CANTON_CLIENTE       VARCHAR(30)          null,
   constraint PK_LUGAR primary key (LUGAR_ID)
);

/*==============================================================*/
/* Index: LUGAR_PK                                              */
/*==============================================================*/
create unique index LUGAR_PK on LUGAR (
LUGAR_ID
);

/*==============================================================*/
/* Table: MANTENIMIENTO                                         */
/*==============================================================*/
create table MANTENIMIENTO (
   MANTENIMIENTO_ID     SERIAL               not null,
   TECNICOS_ID          INT4                 not null,
   CLIENTE_ID           INT4                 not null,
   TIPO_ID              INT4                 not null,
   GARANTIA_MANTENIMIENTO VARCHAR(10)          null,
   FECHA_RECEPCION      VARCHAR(10)          null,
   FECHA_ENTREGA        VARCHAR(10)          null,
   COSTO_MANTENIMIENTO  INT4                 null,
   ESTADO_MANTENIMIENTO VARCHAR(10)          null,
   constraint PK_MANTENIMIENTO primary key (MANTENIMIENTO_ID)
);

/*==============================================================*/
/* Index: MANTENIMIENTO_PK                                      */
/*==============================================================*/
create unique index MANTENIMIENTO_PK on MANTENIMIENTO (
MANTENIMIENTO_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on MANTENIMIENTO (
TECNICOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on MANTENIMIENTO (
CLIENTE_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on MANTENIMIENTO (
TIPO_ID
);

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   PRODUCTOS_ID         SERIAL               not null,
   PROVEEDOR_ID         INT4                 not null,
   NOMBRE_PRODUCTO      VARCHAR(50)          null,
   NUM_SERIE_PRODUCTO   VARCHAR(10)          null,
   FECHA_PRODUCTO       DATE                 null,
   constraint PK_PRODUCTO primary key (PRODUCTOS_ID)
);

/*==============================================================*/
/* Index: PRODUCTO_PK                                           */
/*==============================================================*/
create unique index PRODUCTO_PK on PRODUCTO (
PRODUCTOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on PRODUCTO (
PROVEEDOR_ID
);

/*==============================================================*/
/* Table: PROVEEDOR                                             */
/*==============================================================*/
create table PROVEEDOR (
   PROVEEDOR_ID         SERIAL               not null,
   NOMBRES_PROVEEDOR    VARCHAR(30)          null,
   APELLIDOS_PROVEEDOR  VARCHAR(30)          null,
   CEDULA_PROVEEDOR     VARCHAR(10)          null,
   NUM_TELEFONO_PRO     VARCHAR(10)          null,
   CORREO_PROVEEDOR     VARCHAR(30)          null,
   SITIO_WEB_PROVEEDOR  VARCHAR(30)          null,
   constraint PK_PROVEEDOR primary key (PROVEEDOR_ID)
);

/*==============================================================*/
/* Index: PROVEEDOR_PK                                          */
/*==============================================================*/
create unique index PROVEEDOR_PK on PROVEEDOR (
PROVEEDOR_ID
);

/*==============================================================*/
/* Table: RELATIONSHIP_9                                        */
/*==============================================================*/
create table RELATIONSHIP_9 (
   PRODUCTOS_ID         INT4                 not null,
   VENTAS_ID            INT4                 not null,
   constraint PK_RELATIONSHIP_9 primary key (PRODUCTOS_ID, VENTAS_ID)
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_PK                                     */
/*==============================================================*/
create unique index RELATIONSHIP_9_PK on RELATIONSHIP_9 (
PRODUCTOS_ID,
VENTAS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_12_FK on RELATIONSHIP_9 (
VENTAS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on RELATIONSHIP_9 (
PRODUCTOS_ID
);

/*==============================================================*/
/* Table: TECNICO                                               */
/*==============================================================*/
create table TECNICO (
   TECNICOS_ID          SERIAL               not null,
   TIEMPO_ID            INT4                 not null,
   NOMBRES_TECNICO      VARCHAR(30)          null,
   APELLIDOS_TECNICO    VARCHAR(30)          null,
   CEDULA_TECNICOS      VARCHAR(10)          null,
   NUM_TELEFONO_TECNICO VARCHAR(10)          null,
   MANTENIMIENTO_ANT    VARCHAR(60)          null,
   MANTENIMIENTO_ACT    VARCHAR(60)          null,
   constraint PK_TECNICO primary key (TECNICOS_ID)
);

/*==============================================================*/
/* Index: TECNICO_PK                                            */
/*==============================================================*/
create unique index TECNICO_PK on TECNICO (
TECNICOS_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on TECNICO (
TIEMPO_ID
);

/*==============================================================*/
/* Table: TIEMPO_LABORABLE                                      */
/*==============================================================*/
create table TIEMPO_LABORABLE (
   TIEMPO_ID            SERIAL               not null,
   HORAS_TRABAJO        VARCHAR(5)           null,
   constraint PK_TIEMPO_LABORABLE primary key (TIEMPO_ID)
);

/*==============================================================*/
/* Index: TIEMPO_LABORABLE_PK                                   */
/*==============================================================*/
create unique index TIEMPO_LABORABLE_PK on TIEMPO_LABORABLE (
TIEMPO_ID
);

/*==============================================================*/
/* Table: TIPO_MANTENIMIENTO                                    */
/*==============================================================*/
create table TIPO_MANTENIMIENTO (
   TIPO_ID              SERIAL               not null,
   TIPO_MANTENIMIENTO   VARCHAR(60)          null,
   constraint PK_TIPO_MANTENIMIENTO primary key (TIPO_ID)
);

/*==============================================================*/
/* Index: TIPO_MANTENIMIENTO_PK                                 */
/*==============================================================*/
create unique index TIPO_MANTENIMIENTO_PK on TIPO_MANTENIMIENTO (
TIPO_ID
);

/*==============================================================*/
/* Table: TIPO_PRODUCTO                                         */
/*==============================================================*/
create table TIPO_PRODUCTO (
   TIPOSPRODUC_ID       SERIAL               not null,
   VENTAS_ID            INT4                 not null,
   TIPO_PRODUCTO        VARCHAR(30)          null,
   constraint PK_TIPO_PRODUCTO primary key (TIPOSPRODUC_ID)
);

/*==============================================================*/
/* Index: TIPO_PRODUCTO_PK                                      */
/*==============================================================*/
create unique index TIPO_PRODUCTO_PK on TIPO_PRODUCTO (
TIPOSPRODUC_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on TIPO_PRODUCTO (
VENTAS_ID
);

/*==============================================================*/
/* Table: VENTA                                                 */
/*==============================================================*/
create table VENTA (
   VENTAS_ID            SERIAL               not null,
   GARANTIA_VENTA       VARCHAR(10)          null,
   TOTAL_VENTA          INT4                 null,
   constraint PK_VENTA primary key (VENTAS_ID)
);

/*==============================================================*/
/* Index: VENTA_PK                                              */
/*==============================================================*/
create unique index VENTA_PK on VENTA (
VENTAS_ID
);

alter table CLIENTE
   add constraint FK_CLIENTE_RELATIONS_LUGAR foreign key (LUGAR_ID)
      references LUGAR (LUGAR_ID)
      on delete restrict on update restrict;

alter table CLIENTE
   add constraint FK_CLIENTE_RELATIONS_VENTA foreign key (VENTAS_ID)
      references VENTA (VENTAS_ID)
      on delete restrict on update restrict;

alter table LINE_COMPRA
   add constraint FK_LINE_COM_RELATIONS_PROVEEDO foreign key (PROVEEDOR_ID)
      references PROVEEDOR (PROVEEDOR_ID)
      on delete restrict on update restrict;

alter table LINE_COMPRA
   add constraint FK_LINE_COM_RELATIONS_COMPRA foreign key (COMPRAS_ID)
      references COMPRA (COMPRAS_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_TECNICO foreign key (TECNICOS_ID)
      references TECNICO (TECNICOS_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_CLIENTE foreign key (CLIENTE_ID)
      references CLIENTE (CLIENTE_ID)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIM_RELATIONS_TIPO_MAN foreign key (TIPO_ID)
      references TIPO_MANTENIMIENTO (TIPO_ID)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_RELATIONS_PROVEEDO foreign key (PROVEEDOR_ID)
      references PROVEEDOR (PROVEEDOR_ID)
      on delete restrict on update restrict;

alter table RELATIONSHIP_9
   add constraint FK_RELATION_RELATIONS_VENTA foreign key (VENTAS_ID)
      references VENTA (VENTAS_ID)
      on delete restrict on update restrict;

alter table RELATIONSHIP_9
   add constraint FK_RELATION_RELATIONS_PRODUCTO foreign key (PRODUCTOS_ID)
      references PRODUCTO (PRODUCTOS_ID)
      on delete restrict on update restrict;

alter table TECNICO
   add constraint FK_TECNICO_RELATIONS_TIEMPO_L foreign key (TIEMPO_ID)
      references TIEMPO_LABORABLE (TIEMPO_ID)
      on delete restrict on update restrict;

alter table TIPO_PRODUCTO
   add constraint FK_TIPO_PRO_RELATIONS_VENTA foreign key (VENTAS_ID)
      references VENTA (VENTAS_ID)
      on delete restrict on update restrict;

