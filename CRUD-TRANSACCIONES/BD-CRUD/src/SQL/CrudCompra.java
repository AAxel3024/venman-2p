package SQL;

import java.sql.Connection;
import java.sql.Statement;
import javax.swing.JOptionPane;
import GETSET.CompraGetSet;
import java.sql.ResultSet;

public class CrudCompra extends CONEXSQL{

    java.sql.Statement st;
    ResultSet rs;
    CompraGetSet compra = new CompraGetSet();

    public void insertar(int id, String pais, String ciudad, String canton){
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into lugar(lugar_id,pais_cliente,ciudad_cliente,canton_cliente) "
                            + "values('" + id + "','" + pais + "','" + ciudad + "','" + canton + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void consultar(int idlugar) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from lugar where lugar_id='" + idlugar + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {

                compra.setIdlugar(rs.getInt("lugar_id"));
                compra.setPais(rs.getString("pais_cliente"));
                compra.setCiudad(rs.getString("ciudad_cliente"));
                compra.setCanton(rs.getString("canton_cliente"));
            } else {
                compra.setIdlugar(Integer.parseInt(""));
                compra.setPais("");
                compra.setCiudad("");
                compra.setCanton("");
                JOptionPane.showMessageDialog(null, "No se encontro registro", "Sin regitro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en el sistema busqueda", "Error busqueda", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void actualizar(String lugar_id, String pais_cliente, String ciudad_cliente, String canton_cliente){
        try {
             Connection conexion =conectar();
             st=conexion.createStatement();
             String sql="update lugar set pais_cliente='"+pais_cliente+"',ciudad_cliente='"+ciudad_cliente+"',"
                     + "canton_cliente='"+canton_cliente+"' where lugar_id='"+lugar_id+"'";
             st.executeUpdate(sql);
             JOptionPane.showMessageDialog(null, "Registro actualizado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
             st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Registro no pudo ser actualizado" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

   public void eliminar(String lugar_id){
        try {
            Connection conexion =conectar();
            st=conexion.createStatement();
            String sql="delete from lugar where lugar_id='"+lugar_id+"' ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Registro no eliminado " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
