package SQL;

import java.sql.Connection;
import java.sql.Statement;
import javax.swing.JOptionPane;
import GETSET.ClienteGetSet;
import java.sql.ResultSet;

public class CrudSQL extends CONEXSQL {

    java.sql.Statement st;
    ResultSet rs;
    ClienteGetSet clientes = new ClienteGetSet();

    public void insertar(int id, int lugar, int ventas, String nombre, String apellido, String cedula,
            String telefono, String correo, String genero, String direccion) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into cliente(cliente_id,lugar_id,ventas_id,nombre_cliente,apellido_cliente,cedula_cliente,"
                    + "num_telefono_cliente,correo_cliente,genero_cliente,direccion_cliente) "
                    + "values('" + id + "','" + lugar + "','" + ventas + "','" + nombre + "','" + apellido + "',"
                    + "'" + cedula + "','" + telefono + "','" + correo + "','" + genero + "','" + direccion + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void consultar(int idcliente) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from cliente where cliente_id='" + idcliente + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {

                clientes.setIdcliente(rs.getInt("cliente_id"));
                clientes.setLugar(rs.getInt("lugar_id"));
                clientes.setVentas(rs.getInt("ventas_id"));
                clientes.setNombre(rs.getString("nombre_cliente"));
                clientes.setApellido(rs.getString("apellido_cliente"));
                clientes.setCedula(rs.getString("cedula_cliente"));
                clientes.setTelefono(rs.getString("num_telefono_cliente"));
                clientes.setCorreo(rs.getString("correo_cliente"));
                clientes.setGenero(rs.getString("genero_cliente"));
                clientes.setDireccion(rs.getString("direccion_cliente"));
            } else {
                clientes.setIdcliente(Integer.parseInt(""));
                clientes.setLugar(Integer.parseInt(""));
                clientes.setVentas(Integer.parseInt(""));
                clientes.setNombre("");
                clientes.setApellido("");
                clientes.setCedula("");
                clientes.setTelefono("");
                clientes.setCorreo("");
                clientes.setGenero("");
                clientes.setDireccion("");
                JOptionPane.showMessageDialog(null, "No se encontro registro", "Sin regitro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en el sistema busqueda", "Error busqueda", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void actualizar(String id, String lugar, String ventas, String nombre, String apellido, String cedula,
            String telefono, String correo, String genero, String direccion) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update cliente set lugar_id='" + lugar + "',ventas_id='" + ventas + "',nombre_cliente='" + nombre + "',"
                    + "apellido_cliente='" + apellido + "',cedula_cliente='" + cedula + "',num_telefono_cliente='" + telefono + "',"
                    + "correo_cliente='" + correo + "',genero_cliente='" + genero + "',direccion_cliente='" + direccion + "' "
                    + "where cliente_id='" + id + "'";
            st.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "El registro fue modificado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se modifico" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void eliminar(String id) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "delete from cliente where cliente_id='" + id + "' ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro fue eliminado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se Elimino " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
